import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AddBlogComponent } from './components/add-blog/add-blog.component';
import { GetListBlogComponent } from './components/get-list-blog/get-list-blog.component';
import { GetBlogComponent } from './components/get-blog/get-blog.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'get-list-blog' },
  { path: 'add-blog', component: AddBlogComponent },
  { path: 'get-list-blog', component: GetListBlogComponent },
  { path: 'get-blog/:id', component: GetBlogComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
