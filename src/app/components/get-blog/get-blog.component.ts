import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CrudBlogService } from 'src/app/service/crud-blog.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-get-blog',
  templateUrl: './get-blog.component.html',
  styleUrls: ['./get-blog.component.css']
})
export class GetBlogComponent implements OnInit {
  getBlogForm: FormGroup;
  blogID:any;
  
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private formBlog: FormBuilder,
    private crudBlogService:CrudBlogService,
    private datePipe: DatePipe
  ){
    this.blogID = this.activatedRoute.snapshot.paramMap.get('id');
    console.log(this.blogID);
    
    this.crudBlogService.getBlogService(this.blogID).subscribe(response => {
      console.log(response.blog);
      this.getBlogForm.setValue({
        titulo: response.blog.titulo,
        contenido: response.blog.contenido,
        autor: response.blog.autor,
        fecha: this.datePipe.transform(response.blog.fecha_publicacion, 'yyyy-MM-dd')
      });
    });

    this.getBlogForm = this.formBlog.group({
      titulo: [''],
      contenido: [''],
      autor: [''],
      fecha: ['']
    });
  }

  ngOnInit(): void {
  }

  

}
