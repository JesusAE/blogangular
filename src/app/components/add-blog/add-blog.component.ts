import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CrudBlogService } from 'src/app/service/crud-blog.service';
import { Router } from '@angular/router';
import Swal  from 'sweetalert2';

@Component({
  selector: 'app-add-blog',
  templateUrl: './add-blog.component.html',
  styleUrls: ['./add-blog.component.css']
})
export class AddBlogComponent implements OnInit {
  addBlogForm: FormGroup;

  constructor( 
    public formBuilder:FormBuilder,
    private crudBlogService:CrudBlogService,
    private router:Router){ 
    this.addBlogForm = this.formBuilder.group({
      titulo: [''],
      contenido: [''],
      autor: [''],
      fecha: ['']
    });
  }

  ngOnInit(): void {
  
  }

  addBlog(): any {
    Swal.fire("Guardando registro");
    this.crudBlogService.AddBlogService(this.addBlogForm.value).subscribe((response:any) => {
      if(response){
        let icono:any = "";
        icono = (response.response==200)? "success" : "error";
        Swal.fire({
          title: response.message,
          text: "Presiona Aceptar para continuar",
          icon: icono,
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Aceptar',
          cancelButtonText: 'Cancelar',
          allowOutsideClick: false,
          allowEscapeKey: false
        })
        if(response.response==200)
          this.router.navigateByUrl('/get-list-blog');
      }else{
        Swal.fire(
          'Error!',
          'Ocurrio un error al registrar!',
          'error'
        )
      }
    });
  }
}
