import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CrudBlogService } from 'src/app/service/crud-blog.service';
import { Router } from '@angular/router';
import Swal  from 'sweetalert2';


@Component({
  selector: 'app-get-list-blog',
  templateUrl: './get-list-blog.component.html',
  styleUrls: ['./get-list-blog.component.css']
})

export class GetListBlogComponent implements OnInit {
  Blogs:any;
  searchBlogForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private crudBlogService: CrudBlogService,
    private router: Router) { 
      this.searchBlogForm = this.formBuilder.group({
      search: [''],
    });
  }

  ngOnInit(): void {
    Swal.fire({
      icon: 'success',
      title: 'Cargando registros',
      showConfirmButton: false,
    })
    this.crudBlogService.getListBlogService().subscribe((response:any) => {
      if(response){
        if(response.response==400){
          Swal.fire({
            title: response.message,
            text: "Presiona Aceptar para continuar",
            icon: "error"
          })
        }
        if(response.response==200)
          this.Blogs = response.blogs;
      }else{
        Swal.fire(
          'Error!',
          'Ocurrio un error al consultar registros!',
          'error'
        )
      }
    });
  }

  searchBlog(): any {
    Swal.fire("buscando registros");
    this.crudBlogService.SearchBlogService(this.searchBlogForm.value).subscribe((response:any) => {
      if(response){
        let icono:any = "";
        icono = (response.response==200)? "success" : "error";
        Swal.fire({
          title: response.message,
          text: "Presiona Aceptar para continuar",
          icon: icono,
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Aceptar',
          cancelButtonText: 'Cancelar',
          allowOutsideClick: false,
          allowEscapeKey: false
        })
        if(response.response==200)
          this.Blogs = response.blogs;
      }else{
        Swal.fire(
          'Error!',
          'Ocurrio un error al consultar registros!',
          'error'
        )
      }
    });
  }

}
