import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GetListBlogComponent } from './get-list-blog.component';

describe('GetListBlogComponent', () => {
  let component: GetListBlogComponent;
  let fixture: ComponentFixture<GetListBlogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GetListBlogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GetListBlogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
