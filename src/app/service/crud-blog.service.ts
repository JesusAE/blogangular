import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Blog } from './Blog';

@Injectable({
  providedIn: 'root'
})
export class CrudBlogService {
  API: string = 'http://apiblog.test/api/';
  constructor( private clientHttp: HttpClient ) { 
  }

  AddBlogService(dataBlog:Blog): Observable <any> {
    return this.clientHttp.post(this.API + 'addBlog', dataBlog);
  }

  getListBlogService(){
    return this.clientHttp.get(this.API + 'getBlogs');
  }

  getBlogService(id:any): Observable<any> {
    return this.clientHttp.get(this.API + 'getBlog/' +id);
  }

  SearchBlogService(search:any): Observable <any> {
    return this.clientHttp.post(this.API + 'searchBlogs', search);
  }
}
