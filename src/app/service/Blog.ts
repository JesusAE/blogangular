export class Blog {
    id!: number;
    titulo!: string;
    autor!: string;
    contenido!: string;
    fecha!: Date;
}