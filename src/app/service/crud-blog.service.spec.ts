import { TestBed } from '@angular/core/testing';

import { CrudBlogService } from './crud-blog.service';

describe('CrudBlogService', () => {
  let service: CrudBlogService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CrudBlogService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
